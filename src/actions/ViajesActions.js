import axios from "axios";
import { url } from "../core/connection";
import { NotificationManager } from 'react-notifications';

export const selectAllViajesActivosFunction = () => dispatch => {
    axios.get(`${url}/api/indexViaje`)
        .then(res => {
            dispatch({
                type: "SELECT_VIAJES_ACTIVOS",
                payload: {
                    data: res.data,
                }
            });
        })
        .catch(error => {
            console.log("Error consultando la api para consultar los viajes activos", error.toString());
        });
};

export const saveViajeAction = (data, callback, callbackLoading) => dispatch => {
    axios({
        method: "post",
        url: `${url}/api/createViaje`,
        data: data
    })
        .then(res => {
            NotificationManager.success(res.data.mensaje);
            dispatch({
                type: "ADD_NEW_VIAJE",
                payload: res.data.data
            });
            callback();

        })
        .catch(error => {
            callbackLoading();
            NotificationManager.warning("Error registrando el viaje");
        });
};

export const selectViajeIdAction = (id) => dispatch => {
    axios({
        method: "get",
        url: `${url}/api/indexViajeId/${id}`,
    })
        .then(res => {
            dispatch({
                type: "SELECT_VIAJE_ID",
                payload: {
                    data: res.data,
                }
            });
        })
        .catch(error => {
            console.log(error)
            NotificationManager.warning("Error consultando el viaje por id");
        });
};

export const updateViajeAction = (data, callback, callbackLoading) => dispatch => {
    axios({
        method: "put",
        url: `${url}/api/updateViaje/${data.id}`,
        data: data
    })
        .then(res => {
            NotificationManager.success(res.data.mensaje);
            dispatch({
                type: "UPDATE_VIAJE",
                payload: res.data.data
            });
            callback();
        })
        .catch(error => {
            callbackLoading();
            NotificationManager.warning("Error actualizando el viaje");
        });
};

export const deleteViajeAction = (data) => dispatch => {
    axios({
        method: "put",
        url: `${url}/api/deleteViaje/${data.id}`,
        data: data
    })
        .then(res => {
            NotificationManager.success(res.data.mensaje);
            dispatch({
                type: "DELETE_VIAJE",
                payload: data.id
            });
        })
        .catch(error => {
            NotificationManager.warning("Error eliminando el viaje");
        });
};