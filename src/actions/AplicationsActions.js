export const openConfirmDialog = (message, callback) => {
    return {
        type: "OPEN_CONFIRM",
        payload: {
            message,
            callback
        }
    };
};

export const closeDialog = () => {
    return {
        type: "CLOSE_CONFIRM"
    };
};