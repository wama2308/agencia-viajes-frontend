import axios from "axios";
import { url } from "../core/connection";
import { NotificationManager } from 'react-notifications';

export const selectAllViajerosActivosFunction = () => dispatch => {
    axios.get(`${url}/api/indexViajero`)
        .then(res => {
            loadSelectViajesFunction(dataViajes => {
                dispatch({
                    type: "SELECT_VIAJEROS_ACTIVOS",
                    payload: {
                        data: res.data,
                        dataViajes,
                    }
                });
            });
        })
        .catch(error => {
            console.log("Error consultando la api para consultar los viajeros activos", error.toString());
        });
};

export const selectAllViajerosViajesFunction = (id) => dispatch => {
    axios.get(`${url}/api/viajeroViajesId/${id}`)
        .then(res => {
            dispatch({
                type: "VIAJES_VIAJERO",
                payload: {
                    data: res.data,
                }
            });
        })
        .catch(error => {
            console.log("Error consultando la api para consultar los los viajes del pasajero", error.toString());
        });
};

const loadSelectViajesFunction = (execute) => {
    axios.get(`${url}/api/selectViajes`)
        .then(res => {
            execute(res.data);
        })
        .catch(error => {
            NotificationManager.warning('Error cargando el select viajes');
            console.log('Error cargando el select viajes')
        });
};

export const saveViajeroAction = (data, callback, callbackLoading) => dispatch => {
    axios({
        method: "post",
        url: `${url}/api/createViajero`,
        data: data
    })
        .then(res => {
            NotificationManager.success(res.data.mensaje);
            dispatch({
                type: "ADD_NEW_VIAJERO",
                payload: res.data.data
            });
            callback();
        })
        .catch(error => {
            callbackLoading();
            NotificationManager.warning("Error registrando el viajero");
        });
};

export const selectViajeroIdAction = (id) => dispatch => {
    axios({
        method: "get",
        url: `${url}/api/indexViajeroId/${id}`,
    })
        .then(res => {
            dispatch({
                type: "SELECT_VIAJERO_ID",
                payload: {
                    data: res.data,
                }
            });
        })
        .catch(error => {
            console.log(error)
            NotificationManager.warning("Error consultando el viajero por id");
        });
};

export const updateViajeroAction = (data, callback, callbackLoading) => dispatch => {
    axios({
        method: "put",
        url: `${url}/api/updateViajero/${data.id}`,
        data: data
    })
        .then(res => {
            NotificationManager.success(res.data.mensaje);
            dispatch({
                type: "UPDATE_VIAJERO",
                payload: res.data.data
            });
            callback();
        })
        .catch(error => {
            callbackLoading();
            NotificationManager.warning("Error actualizando el viajero");
        });
};

export const deleteViajeroAction = (data) => dispatch => {
    axios({
        method: "put",
        url: `${url}/api/deleteViajero/${data.id}`,
        data: data
    })
        .then(res => {
            NotificationManager.success(res.data.mensaje);
            dispatch({
                type: "DELETE_VIAJERO",
                payload: data.id
            });
        })
        .catch(error => {
            NotificationManager.warning("Error eliminando el viajero");
        });
};

export const asignarViajesAction = (data) => dispatch => {
    dispatch({
        type: "ASIGNAR_VIAJE",
        payload: data
    });
}

export const deleteViajeAsignadoAction = (index) => dispatch => {
    dispatch({
        type: "DELETE_VIAJE_ASIGNADO",
        payload: index
    });
}

export const cleanViajeAsignadoAction = (index) => dispatch => {
    dispatch({
        type: "CLEAN_VIAJES_ASIGNADO",
        payload: []
    });
}

export const saveViajeroViajesAction = (data, callback) => {
    axios({
        method: "post",
        url: `${url}/api/createViajeroViajes`,
        data: data
    })
        .then(res => {
            NotificationManager.success(res.data.mensaje);
            callback();
        })
        .catch(error => {
            callback();
            NotificationManager.warning("Error registrando los viajes del pasajero");
        });
};