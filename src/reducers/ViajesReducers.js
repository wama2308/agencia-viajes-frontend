const InitalState = {
    data: [],
    viajeId: [],
};

const addNewViajeFunction = (state, payload) => {
    let dataState = state.data;
    let estado = state;
    let data = [...dataState, payload];
    estado.data = data;
    return estado;
}

const updateViajeFunction = (state, payload) => {
    let estado = state;
    let dataArray = state.data;
    let indexData = dataArray.findIndex(dataViaje => dataViaje.id === payload.id);
    let dataUpdate = [...dataArray.splice(indexData, 1, payload)];
    return estado
}

const ViajesReducers = (state = InitalState, action) => {
    switch (action.type) {
        case 'SELECT_VIAJES_ACTIVOS': {
            return {
                ...state,
                data: [...action.payload.data],
            };
        }
        case 'ADD_NEW_VIAJE': {
            return addNewViajeFunction(state, action.payload)
        }
        case 'SELECT_VIAJE_ID': {
            return {
                ...state,
                viajeId: action.payload.data,
            };
        }
        case 'UPDATE_VIAJE': {
            return updateViajeFunction(state, action.payload)
        }
        case 'DELETE_VIAJE': {
            return {
                ...state,
                data: state.data.filter(dataViaje => dataViaje.id !== action.payload)
            }
        }
        default:
            return state;
    }
};

export default ViajesReducers;