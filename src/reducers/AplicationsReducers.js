const InitalState = {
    confirm: {
        open: false,
        message: "",
        callback: undefined
    },
};

const aplicationReducers = (state = InitalState, action) => {
    switch (action.type) {
        case "OPEN_CONFIRM": {
            return {
                ...state,
                confirm: {
                    open: true,
                    ...action.payload.message,
                    callback: action.payload.callback
                }
            };
        }
        case "CLOSE_CONFIRM": {
            return { ...state, confirm: { ...state.confirm, open: false } };
        }
        default:
            return state;
    }
};

export default aplicationReducers;
