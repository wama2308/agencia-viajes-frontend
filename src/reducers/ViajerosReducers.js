
const InitalState = {
    data: [],
    viajes: [],
    viajeroId: {},
    viajesAsignados: [],
    viajesGuardados: [],
};

const addNewViajeroFunction = (state, payload) => {
    let dataState = state.data;
    let estado = state;
    let data = [...dataState, payload];
    estado.data = data;
    return estado;
}

const updateViajeroFunction = (state, payload) => {
    let estado = state;
    let dataArray = state.data;
    let indexData = dataArray.findIndex(dataViajero => dataViajero.id === payload.id);
    let dataUpdate = [...dataArray.splice(indexData, 1, payload)];
    return estado
}

const ViajerosReducers = (state = InitalState, action) => {
    switch (action.type) {
        case 'SELECT_VIAJEROS_ACTIVOS': {
            return {
                ...state,
                data: [...action.payload.data],
                viajes: [...action.payload.dataViajes],
            };
        }
        case 'ADD_NEW_VIAJERO': {
            return addNewViajeroFunction(state, action.payload)
        }
        case 'SELECT_VIAJERO_ID': {
            return {
                ...state,
                viajeroId: action.payload.data,
            };
        }
        case 'UPDATE_VIAJERO': {
            return updateViajeroFunction(state, action.payload)
        }
        case 'DELETE_VIAJERO': {
            return {
                ...state,
                data: state.data.filter(dataViajero => dataViajero.id !== action.payload)
            }
        }
        case 'ASIGNAR_VIAJE': {
            return {
                ...state,
                viajesAsignados: [...state.viajesAsignados, action.payload]
            };
        }
        case 'DELETE_VIAJE_ASIGNADO': {
            return {
                ...state,
                viajesAsignados: state.viajesAsignados.slice(0, action.payload).concat(state.viajesAsignados.slice(action.payload + 1))
            };
        }
        case 'CLEAN_VIAJES_ASIGNADO': {
            return {
                ...state,
                viajesAsignados: action.payload
            };
        }
        case 'VIAJES_VIAJERO': {
            return {
                ...state,
                viajesGuardados: [...action.payload.data]
            };
        }
        default:
            return state;
    }
};

export default ViajerosReducers;