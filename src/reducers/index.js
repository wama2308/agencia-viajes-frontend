import { combineReducers } from "redux";
import ViajesReducers from "./ViajesReducers";
import ViajerosReducers from "./ViajerosReducers";
import AplicationsReducers from "./AplicationsReducers";

const reducers = combineReducers({
  aplications: AplicationsReducers,
  viajes: ViajesReducers,
  viajeros: ViajerosReducers,
});

export default reducers;
