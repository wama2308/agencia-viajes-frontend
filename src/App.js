import React from "react";
import { Route } from "react-router-dom";
import { connect } from 'react-redux';
import DefaultLayout from "./container/DefaultLayout";
import { closeDialog } from "./actions/AplicationsActions";
import { Alert } from "./components/Confirm";
import { NotificationContainer } from 'react-notifications';
import 'react-notifications/lib/notifications.css';

const App = (props) => {
  const { match } = props;
  return (
    <div>
      <Alert {...props.alertConfirm} close={props.closeDialog} />
      <NotificationContainer />
      <Route
        path={`${match.url}`}
        render={(props) =>
          <DefaultLayout {...props} />
        }
      />
    </div>
  );
}

const mapStateToProps = state => ({
  alertConfirm: state.aplications.confirm,
});

const mapDispatchToProps = dispatch => ({
  closeDialog: () => dispatch(closeDialog()),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(App);