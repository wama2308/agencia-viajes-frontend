import DefaultLayout from "./container/DefaultLayout";
import Viajeros from "./views/viajeros/ListViajeros";
import Viajes from "./views/viajes/ListViajes";

const routes = [
    {
      path: "/",
      exact: true,
      name: "Home",
      component: DefaultLayout
    },    
    {
      path: "/viajeros",
      exact: true,
      name: "Viajeros",
      component: Viajeros
    },
    {
      path: "/viajes",
      exact: true,
      name: "Viajes",
      component: Viajes
    }
];

export default routes;