import React, { Component } from "react";
import {
    Button,
    Dialog,
    DialogActions,
    DialogContent,
    DialogTitle,
} from '@material-ui/core';

export class Alert extends React.Component {
    handleOk = () => {
        this.props.callback(true);
        this.props.close()
    };

    handleNotOk = () => {
        this.props.callback(false);
        this.props.close()
    };

    render() {
        return (
            <Dialog
                fullWidth={true}
                maxWidth="sm"
                open={this.props.open}
                onClose={this.closeModal}
                aria-labelledby="responsive-dialog-title"
            >
                <DialogTitle id="form-dialog-title">{this.props.title}</DialogTitle>
                <DialogContent dividers>{this.props.info}{" "}</DialogContent>
                <DialogActions>
                    <Button variant="contained" color="default" onClick={this.handleNotOk}>
                        No
                    </Button>
                    <Button variant="contained" color="primary" onClick={this.handleOk}>
                        Si
                    </Button>{" "}

                </DialogActions>
            </Dialog>
        );
    }
}
