import React from 'react';
import { Link } from 'react-router-dom';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import EmojiPeopleIcon from '@material-ui/icons/EmojiPeople';
import FlightIcon from '@material-ui/icons/Flight';

export const mainListItems = (
  <div>
    <ListItem button component={Link} to="/viajeros">
      <ListItemIcon>
        <EmojiPeopleIcon />
      </ListItemIcon>
      <ListItemText primary="Viajeros" />
    </ListItem>
    <ListItem button component={Link} to="/viajes">
      <ListItemIcon>
        <FlightIcon />
      </ListItemIcon>
      <ListItemText primary="Viajes" />
    </ListItem>    
  </div>
);