import React, { useState, useEffect } from 'react';
import "../../assets/css/style.css";
import { stateInitial } from './StateInitial';
import {
    CircularProgress,
    Dialog,
    DialogActions,
    DialogContent,
    DialogTitle,
    IconButton,
    FormControl,
    TextField,
    Button,
} from "@material-ui/core";
import { Close } from "@material-ui/icons";
import { formatMonto, number_format } from "../../core/helpers";
import { connect } from "react-redux";
import { saveViajeAction, updateViajeAction } from "../../actions/ViajesActions"

const DialogViajes = props => {
    const initialFormState = stateInitial;
    const [modal, setModal] = useState(initialFormState)

    useEffect(() => {
        if (props.option === 1) {
            setModal(prev => ({ ...prev, loading: "hide" }))
        } else if (props.option === 2 || props.option === 3) {
            if (props.data && Object.keys(props.data).length > 0 && modal.actionReducer === 0) {
                cargarData(props.data);
            }
        }
    }, [props])

    const cargarData = (data) => {
        setModal(prev => ({
            ...prev,
            codigo: data.codigo_viaje,
            nroPlazas: data.numero_plazas,
            origen: data.origen,
            destino: data.destino,
            precio: number_format(data.precio, 2),
            loading: 'hide',
            actionReducer: 1,
        }));
    }

    const handleChange = (error, textError) => e => {
        const { id, value } = e.target;
        setModal(prev => ({
            ...prev,
            [id]: value,
            [error]: false,
            [textError]: "",
        }))
    };

    const handleChangeMonto = (error, textError) => e => {
        const { id, value } = e.target;
        let monto = value.replace(/\D/g, "")
            .replace(/([0-9])([0-9]{2})$/, '$1.$2')
            .replace(/\B(?=(\d{3})+(?!\d)\.?)/g, ",");
        setModal(prev => ({
            ...prev,
            [id]: monto,
            [error]: false,
            [textError]: "",
        }))
    };

    const eventoBlur = e => {
        const { id, value } = e.target;
        if (value === '' || value === '0.0') {
            setModal(prev => ({
                ...prev,
                [id]: '0.00'
            }))
        }
    }

    const eventoFocus = e => {
        const { id, value } = e.target;
        if (value === '0.00') {
            setModal(prev => ({
                ...prev,
                [id]: ''
            }))
        }
    }

    const closeModal = () => {
        setModal({
            ...initialFormState
        });
        props.valorCloseModal();
    };

    const loadingShowAction = () => {
        setModal(prev => ({
            ...prev,
            loading: "hide"
        }));
    }

    const validate = () => {
        let acum = "";
        if (modal.codigo === '') {
            setModal(prev => ({
                ...prev,
                codigoError: true,
                codigoTextError: "Ingrese el codigo",
            }))
            acum = 1;
        }
        if (modal.nroPlazas === '') {
            setModal(prev => ({
                ...prev,
                nroPlazasError: true,
                nroPlazasTextError: "Ingrese el numero de plazas",
            }))
            acum = 1;
        }
        if (modal.origen === '') {
            setModal(prev => ({
                ...prev,
                origenError: true,
                origenTextError: "Ingrese el lugar de origen",
            }))
            acum = 1;
        }
        if (modal.destino === '') {
            setModal(prev => ({
                ...prev,
                destinoError: true,
                destinoTextError: "Ingrese el lugar de destino",
            }))
            acum = 1;
        }
        if (modal.precio === '0.00') {
            setModal(prev => ({
                ...prev,
                precioError: true,
                precioTextError: "Ingrese el precio",
            }))
            acum = 1;
        }
        if (acum > 0) {
            return false;
        }
        return true;
    }

    const handleSaveViaje = event => {
        event.preventDefault();
        const isValid = validate();
        if (isValid) {
            setModal(prev => ({
                ...prev,
                loading: "show"
            }));
            let dataSend = {
                id: props.data ? props.data.id : 0,
                codigo_viaje: modal.codigo,
                numero_plazas: modal.nroPlazas,
                origen: modal.origen,
                destino: modal.destino,
                precio: formatMonto(modal.precio),
            }
            if (props.option === 1) {
                props.saveViajeAction(dataSend, () => { closeModal() }, () => { loadingShowAction() });
            }
            if (props.option === 3) {
                props.updateViajeAction(dataSend, () => { closeModal() }, () => { loadingShowAction() });
            }
        }
    }

    return (
        <Dialog
            fullWidth={true}
            maxWidth="sm"
            open={props.modal}
            onClose={() => { closeModal() }}
            aria-labelledby="responsive-dialog-title"
            scroll="paper"
        >
            {modal.loading === "hide" ? (
                <div>
                    <DialogTitle id="form-dialog-title">
                        <div style={{ display: 'flex' }}>
                            <div>
                                {props.modalHeader}
                            </div>
                            <div style={{ marginLeft: 'auto' }}>
                                <IconButton aria-label="Delete"
                                    className="iconButtons"
                                    onClick={() => { closeModal() }}
                                >
                                    <Close className="iconTable" />
                                </IconButton>
                            </div>
                        </div>
                    </DialogTitle>
                    <DialogContent dividers>
                        <FormControl fullWidth>
                            <div>
                                <TextField
                                    error={modal.codigoError}
                                    id="codigo"
                                    label="Codigo"
                                    helperText={modal.codigoTextError}
                                    value={modal.codigo}
                                    variant="outlined"
                                    size="small"
                                    onChange={handleChange('codigoError', 'codigoTextError')}
                                    fullWidth
                                    type='text'
                                    disabled={props.option !== 2 ? false : true}
                                />
                            </div>
                            <div style={{ marginTop: 20 }}>
                                <TextField
                                    error={modal.nroPlazasError}
                                    id="nroPlazas"
                                    label="Numero de Plazas"
                                    helperText={modal.nroPlazasTextError}
                                    value={modal.nroPlazas}
                                    variant="outlined"
                                    size="small"
                                    onChange={handleChange('nroPlazasError', 'nroPlazasTextError')}
                                    fullWidth
                                    type='number'
                                    disabled={props.option !== 2 ? false : true}
                                />
                            </div>
                            <div style={{ marginTop: 20 }}>
                                <TextField
                                    error={modal.origenError}
                                    id="origen"
                                    label="Lugar Origen"
                                    helperText={modal.origenTextError}
                                    value={modal.origen}
                                    variant="outlined"
                                    size="small"
                                    onChange={handleChange('origenError', 'origenTextError')}
                                    fullWidth
                                    type='text'
                                    disabled={props.option !== 2 ? false : true}
                                />
                            </div>
                            <div style={{ marginTop: 20 }}>
                                <TextField
                                    error={modal.destinoError}
                                    id="destino"
                                    label="Lugar Destino"
                                    helperText={modal.destinoTextError}
                                    value={modal.destino}
                                    variant="outlined"
                                    size="small"
                                    onChange={handleChange('destinoError', 'destinoTextError')}
                                    fullWidth
                                    type='text'
                                    disabled={props.option !== 2 ? false : true}
                                />
                            </div>
                            <div style={{ marginTop: 20 }}>
                                <TextField
                                    error={modal.precioError}
                                    id="precio"
                                    label="Precio"
                                    helperText={modal.precioTextError}
                                    value={modal.precio}
                                    variant="outlined"
                                    size="small"
                                    onChange={handleChangeMonto('precioError', 'precioTextError')}
                                    onBlur={eventoBlur}
                                    onFocus={eventoFocus}
                                    fullWidth
                                    type='text'
                                    disabled={props.option !== 2 ? false : true}
                                />
                            </div>
                        </FormControl>
                    </DialogContent>
                    <DialogActions>
                        <Button
                            variant="contained"
                            color="default"
                            onClick={() => closeModal()}
                        >
                            Cancelar
                        </Button>
                        {
                            !props.showHide &&
                            <Button
                                variant="contained"
                                color="primary"
                                onClick={handleSaveViaje}
                            >
                                {props.buttonFooter}
                            </Button>
                        }

                    </DialogActions>
                </div>
            ) : (
                    <div style={{ height: "55vh" }}>
                        <CircularProgress
                            style={{
                                position: "fixed",
                                height: 40,
                                top: "45%",
                                right: "50%",
                                zIndex: 2
                            }}
                        />
                    </div>
                )
            }
        </Dialog>
    );
}

const mapStateToProps = state => ({
    viajes: state.viajes,
});

const mapDispatchToProps = dispatch => ({
    saveViajeAction: (data, callback, callbackLoading) => dispatch(saveViajeAction(data, callback, callbackLoading)),
    updateViajeAction: (data, callback, callbackLoading) => dispatch(updateViajeAction(data, callback, callbackLoading)),
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(DialogViajes);