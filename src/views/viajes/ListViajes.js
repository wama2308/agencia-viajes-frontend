import React, { Fragment, useState, useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import {
    CircularProgress,
    Button,
    Typography,
    Table,
    TableBody,
    TableCell,
    TableHead,
    TableRow,
    IconButton,
} from '@material-ui/core';
import { Edit, Visibility, Delete } from "@material-ui/icons";
import DialogViajes from "./DialogViajes";
import { selectAllViajesActivosFunction, selectViajeIdAction, deleteViajeAction } from "../../actions/ViajesActions"
import { openConfirmDialog } from "../../actions/AplicationsActions"
import { connect } from "react-redux";
import { number_format } from "../../core/helpers";


const useStyles = makeStyles((theme) => ({
    root: {
        flex: 1,
    },
}));

const Viajes = (props) => {
    const classes = useStyles();
    const initialState = {
        modal: false,
        modalHeader: '',
        modalFooter: '',
        buttonFooter: '',
        data: {},
        disabled: '',
        showHide: false,
        option: 0,
    }
    const [list, setList] = useState(initialState)

    const valorCloseModal = () => {
        setList({ modal: false })
    }

    useEffect(() => {
        props.selectAllViajesActivosFunction();
    }, [])

    const openModal = (option, data) => {
        if (option === 1) {
            setList({
                option: option,
                modal: true,
                modalHeader: 'Registrar Viaje',
                buttonFooter: 'Guardar',
                disabled: false,
                showHide: false,
                data: data,
            })
        }
        else if (option === 2) {
            props.selectViajeIdAction(data.id)
            setList({
                option: option,
                modal: true,
                modalHeader: 'Ver Viaje',
                buttonFooter: '',
                disabled: true,
                showHide: true,
                data: data,
            })
        }
        else if (option === 3) {
            props.selectViajeIdAction(data.id)
            setList({
                option: option,
                modal: true,
                modalHeader: 'Editar Viaje',
                buttonFooter: 'Editar',
                disabled: false,
                showHide: false,
                data: data,
            })
        }
    }

    const deleteViaje = (data) => {
        const message = {
            title: "Eliminar Viaje",
            info: "Los viajeros asociados al viaje tambien seran eliminados ¿Esta seguro que desea eliminar el viaje?"
        };
        props.confirm(message, res => {
            if (res) {
                props.deleteViajeAction(data)
            }
        });
    }

    return (
        <div className={classes.root}>

            {
                list.modal &&
                <DialogViajes
                    option={list.option}
                    modal={list.modal}
                    modalHeader={list.modalHeader}
                    buttonFooter={list.buttonFooter}
                    disabled={list.disabled}
                    showHide={list.showHide}
                    data={list.data}
                    valorCloseModal={valorCloseModal}
                />
            }
            <div>
                <Typography variant="h5" color="textSecondary" align="center">Lista de Viajes</Typography>
            </div>
            <div style={{ paddingTop: 20 }}>
                <Button
                    variant="contained"
                    color="primary"
                    onClick={() => { openModal(1); }}
                >
                    Agregar
                </Button>
            </div>
            {
                props.viajes.data ?
                    <div style={{ paddingTop: 20 }}>
                        <Table aria-label="a dense table">
                            <TableHead className="">
                                <TableRow hover>
                                    <TableCell style={{ width: '10%' }} align="left">Nro</TableCell>
                                    <TableCell style={{ width: '15%' }} align="left">Codigo</TableCell>
                                    <TableCell style={{ width: '15%' }} align="left">Nro de Plazas</TableCell>
                                    <TableCell style={{ width: '15%' }} align="left">Origen</TableCell>
                                    <TableCell style={{ width: '15%' }} align="left">Destino</TableCell>
                                    <TableCell style={{ width: '10%' }} align="left">Precio ($)</TableCell>
                                    <TableCell style={{ width: '20%' }} align="left">Acciones</TableCell>
                                </TableRow>
                            </TableHead>
                            <TableBody>
                                <Fragment>
                                    {props.viajes.data ? props.viajes.data.map((data, i) => {
                                        return (
                                            <TableRow hover key={i}>
                                                <TableCell style={{ width: '10%' }} align="left">{i + 1}</TableCell>
                                                <TableCell style={{ width: '15%' }} align="left">{data.codigo_viaje}</TableCell>
                                                <TableCell style={{ width: '15%' }} align="left">{data.numero_plazas}</TableCell>
                                                <TableCell style={{ width: '15%' }} align="left">{data.origen}</TableCell>
                                                <TableCell style={{ width: '15%' }} align="left">{data.destino}</TableCell>
                                                <TableCell style={{ width: '10%' }} align="left">{number_format(data.precio, 2)}</TableCell>
                                                <TableCell style={{ width: '20%', minWidth: '200px' }} align="left">
                                                    <div className="" style={{ marginTop: '-24px', height: '10px' }}>
                                                        <IconButton aria-label="Delete"
                                                            title="Ver Viaje"
                                                            className="iconButtons"
                                                            onClick={() => { openModal(2, data); }}
                                                        >
                                                            <Visibility className="iconTable" />
                                                        </IconButton>
                                                        <IconButton aria-label="Delete"
                                                            title="Editar Viaje"
                                                            className="iconButtons"
                                                            onClick={() => { openModal(3, data); }}
                                                        >
                                                            <Edit className="iconTable" />
                                                        </IconButton>
                                                        <IconButton aria-label="Delete"
                                                            title="Eliminar Viajero"
                                                            className="iconButtons"
                                                            onClick={() => { deleteViaje(data); }}
                                                        >
                                                            <Delete className="iconTable" />
                                                        </IconButton>
                                                    </div>
                                                </TableCell>
                                            </TableRow>
                                        );
                                    })
                                        :
                                        null
                                    }
                                </Fragment>
                            </TableBody>
                        </Table>
                    </div>
                    :
                    <div style={{ display: 'flex', justifyContent: 'center', alignItems: 'center', height: "60vh" }}>
                        <CircularProgress style={{ height: 60, width: 60, zIndex: 2 }} />
                    </div>
            }
        </div>
    );
}

const mapStateToProps = state => ({
    viajes: state.viajes,
});

const mapDispatchToProps = dispatch => ({
    selectAllViajesActivosFunction: () => dispatch(selectAllViajesActivosFunction()),
    selectViajeIdAction: (id) => dispatch(selectViajeIdAction(id)),
    deleteViajeAction: (id) => dispatch(deleteViajeAction(id)),
    confirm: (message, callback) => dispatch(openConfirmDialog(message, callback)),
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Viajes);