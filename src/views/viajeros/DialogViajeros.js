import React, { useState, useEffect } from 'react';
import "../../assets/css/style.css";
import { stateInitial } from './StateInitial';
import {
    CircularProgress,
    Dialog,
    DialogActions,
    DialogContent,
    DialogTitle,
    IconButton,
    FormControl,
    TextField,
    Button,
} from "@material-ui/core";
import { Close } from "@material-ui/icons";
import { connect } from "react-redux";
import { saveViajeroAction, updateViajeroAction } from "../../actions/ViajerosActions"

const DialogViajeros = props => {
    const initialFormState = stateInitial;
    const [modal, setModal] = useState(initialFormState)

    useEffect(() => {
        if (props.option === 1) {
            setModal(prev => ({ ...prev, loading: "hide" }))
        } else if (props.option === 2 || props.option === 3) {
            if (props.data && Object.keys(props.data).length > 0 && modal.actionReducer === 0) {
                cargarData(props.data);
            }
        }
    }, [props])

    const cargarData = (data) => {
        setModal(prev => ({
            ...prev,
            cedula: data.cedula,
            nombres: data.nombre,
            fechaNacimiento: data.fecha_nacimiento,
            telefono: data.telefono,            
            loading: 'hide',
            actionReducer: 1,
        }));
    }

    const handleChange = (error, textError) => e => {
        const { id, value } = e.target;        
        setModal(prev => ({
            ...prev,
            [id]: value,
            [error]: false,
            [textError]: "",
        }))
    };



    const closeModal = () => {
        setModal({
            ...initialFormState
        });
        props.valorCloseModal();
    };

    const loadingShowAction = () => {
        setModal(prev => ({
            ...prev,
            loading: "hide"
        }));
    }

    const validate = () => {
        let acum = "";
        if (modal.cedula === '') {
            setModal(prev => ({
                ...prev,
                cedulaError: true,
                cedulaTextError: "Ingrese la cedula",
            }))
            acum = 1;
        }
        if (modal.nombres === '') {
            setModal(prev => ({
                ...prev,
                nombresError: true,
                nombresTextError: "Ingrese nombres",
            }))
            acum = 1;
        }
        if (modal.fechaNacimiento === '') {
            setModal(prev => ({
                ...prev,
                fechaNacimientoError: true,
                fechaNacimientoTextError: "Ingrese la fecha de nacimiento",
            }))
            acum = 1;
        }
        if (modal.telefono === '') {
            setModal(prev => ({
                ...prev,
                telefonoError: true,
                telefonoTextError: "Ingrese el telefono",
            }))
            acum = 1;
        }        
        if (acum > 0) {
            return false;
        }
        return true;
    }

    const handleSaveViajero = event => {
        event.preventDefault();
        const isValid = validate();
        if (isValid) {
            setModal(prev => ({
                ...prev,
                loading: "show"
            }));
            let dataSend = {
                id: props.data ? props.data.id : 0,
                cedula: modal.cedula,
                nombre: modal.nombres,
                fecha_nacimiento: modal.fechaNacimiento,
                telefono: modal.telefono,                
            }
            if (props.option === 1) {
                props.saveViajeroAction(dataSend, () => { closeModal() }, () => { loadingShowAction() });
            }
            if (props.option === 3) {
                props.updateViajeroAction(dataSend, () => { closeModal() }, () => { loadingShowAction() });
            }
        }
    }    

    return (
        <Dialog
            fullWidth={true}
            maxWidth="sm"
            open={props.modal}
            onClose={() => { closeModal() }}
            aria-labelledby="responsive-dialog-title"
            scroll="paper"
        >
            {modal.loading === "hide" ? (
                <div>
                    <DialogTitle id="form-dialog-title">
                        <div style={{ display: 'flex' }}>
                            <div>
                                {props.modalHeader}
                            </div>
                            <div style={{ marginLeft: 'auto' }}>
                                <IconButton aria-label="Delete"
                                    className="iconButtons"
                                    onClick={() => { closeModal() }}
                                >
                                    <Close className="iconTable" />
                                </IconButton>
                            </div>
                        </div>
                    </DialogTitle>
                    <DialogContent dividers>
                        <FormControl fullWidth>
                            <div>
                                <TextField
                                    error={modal.cedulaError}
                                    id="cedula"
                                    label="Cedula"
                                    helperText={modal.cedulaTextError}
                                    value={modal.cedula}
                                    variant="outlined"
                                    size="small"
                                    onChange={handleChange('cedulaError', 'cedulaTextError')}
                                    fullWidth
                                    type='text'
                                    disabled={props.option !== 2 ? false : true}
                                />
                            </div>
                            <div style={{ marginTop: 20 }}>
                                <TextField
                                    error={modal.nombresError}
                                    id="nombres"
                                    label="Nombres"
                                    helperText={modal.nombresTextError}
                                    value={modal.nombres}
                                    variant="outlined"
                                    size="small"
                                    onChange={handleChange('nombresError', 'nombresTextError')}
                                    fullWidth
                                    type='text'
                                    disabled={props.option !== 2 ? false : true}
                                />
                            </div>
                            <div style={{ marginTop: 20 }}>
                                <TextField
                                    error={modal.fechaNacimientoError}
                                    helperText={modal.fechaNacimientoTextError}
                                    id="fechaNacimiento"
                                    label="Fecha de Nacimiento"
                                    type="date"
                                    //defaultValue="2017-05-24"
                                    value={modal.fechaNacimiento}
                                    variant="outlined"
                                    size="small"
                                    onChange={handleChange('fechaNacimientoError', 'fechaNacimientoTextError')}
                                    fullWidth                                    
                                    InputLabelProps={{
                                        shrink: true,
                                    }}
                                    disabled={props.option !== 2 ? false : true}
                                />
                            </div>
                            <div style={{ marginTop: 20 }}>
                                <TextField
                                    error={modal.telefonoError}
                                    id="telefono"
                                    label="Telefono"
                                    helperText={modal.telefonoTextError}
                                    value={modal.telefono}
                                    variant="outlined"
                                    size="small"
                                    onChange={handleChange('telefonoError', 'telefonoTextError')}
                                    fullWidth
                                    type='text'
                                    disabled={props.option !== 2 ? false : true}
                                />
                            </div>
                        </FormControl>
                    </DialogContent>
                    <DialogActions>
                        <Button
                            variant="contained"
                            color="default"
                            onClick={() => closeModal()}
                        >
                            Cancelar
                        </Button>
                        {
                            !props.showHide &&
                            <Button
                                variant="contained"
                                color="primary"
                                onClick={handleSaveViajero}
                            >
                                {props.buttonFooter}
                            </Button>
                        }

                    </DialogActions>
                </div>
            ) : (
                    <div style={{ height: "55vh" }}>
                        <CircularProgress
                            style={{
                                position: "fixed",
                                height: 40,
                                top: "45%",
                                right: "50%",
                                zIndex: 2
                            }}
                        />
                    </div>
                )
            }
        </Dialog>
    );
}

const mapStateToProps = state => ({
    viajeros: state.viajeros,
});

const mapDispatchToProps = dispatch => ({
    saveViajeroAction: (data, callback, callbackLoading) => dispatch(saveViajeroAction(data, callback, callbackLoading)),
    updateViajeroAction: (data, callback, callbackLoading) => dispatch(updateViajeroAction(data, callback, callbackLoading)),
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(DialogViajeros);