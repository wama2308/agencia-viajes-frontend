import React, { Fragment } from 'react';
import {
    Typography,
    Table,
    TableBody,
    TableCell,
    TableHead,
    TableRow,
} from "@material-ui/core";
import { number_format } from "../../core/helpers";

const ListViajesAsignados = (props) => {
    return (
        <div style={{
            minHeight: '40vh',
            maxHeight: '70vh',
            padding: 15,
        }}>
            <Typography variant="h6" color="textSecondary" align="center">Lista de Viajes Asignados</Typography>
            <Table aria-label="a dense table">
                <TableHead className="">
                    <TableRow hover>
                        <TableCell style={{ width: '10%' }} align="left">Nro</TableCell>
                        <TableCell style={{ width: '15%' }} align="left">Codigo</TableCell>
                        <TableCell style={{ width: '30%' }} align="left">Origen</TableCell>
                        <TableCell style={{ width: '30%' }} align="left">Destino</TableCell>
                        <TableCell style={{ width: '15%' }} align="left">Precio($)</TableCell>
                    </TableRow>
                </TableHead>
                <TableBody>
                    <Fragment>
                        {props.dataViajeroViajes ? props.dataViajeroViajes.map((data, i) => {
                            return (
                                <TableRow hover key={i}>
                                    <TableCell style={{ width: '10%' }} align="left">{i + 1}</TableCell>
                                    <TableCell style={{ width: '15%' }} align="left">{data.codigo_viaje}</TableCell>
                                    <TableCell style={{ width: '30%' }} align="left">{data.origen}</TableCell>
                                    <TableCell style={{ width: '30%' }} align="left">{data.destino}</TableCell>
                                    <TableCell style={{ width: '15%' }} align="left">{number_format(data.precio, 2)}</TableCell>
                                </TableRow>
                            );
                        })
                            :
                            null
                        }
                    </Fragment>
                </TableBody>
            </Table>
        </div>
    );
}

export default ListViajesAsignados;