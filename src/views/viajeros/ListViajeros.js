import React, { Fragment, useState, useEffect, useRef } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import {
    CircularProgress,
    Button,
    Typography,
    Table,
    TableBody,
    TableCell,
    TableHead,
    TableRow,
    IconButton,
} from '@material-ui/core';
import { Edit, Visibility, Delete, LocalAirport } from "@material-ui/icons";
import DialogViajeros from "./DialogViajeros";
import DialogAsignarViaje from "./DialogAsignarViaje";
import { selectAllViajerosActivosFunction, selectViajeroIdAction, deleteViajeroAction } from "../../actions/ViajerosActions"
import { openConfirmDialog } from "../../actions/AplicationsActions"
import { connect } from "react-redux";

const useStyles = makeStyles((theme) => ({
    root: {
        flex: 1,
    },
}));

const Viajeros = (props) => {
    const classes = useStyles();
    const initialState = {
        modal: false,
        modalAsignarViaje: false,
        modalHeader: '',
        modalFooter: '',
        buttonFooter: '',
        data: {},
        disabled: '',
        showHide: false,
        option: 0,
    }
    const [list, setList] = useState(initialState)

    const valorCloseModal = () => {
        setList({ modal: false })
    }

    const valorCloseModalAsignarViaje = () => {
        setList({ modalAsignarViaje: false })
    }

    useEffect(() => {
        props.selectAllViajerosActivosFunction();
    }, [])

    const openModal = (option, data) => {
        if (option === 1) {
            setList({
                option: option,
                modal: true,
                modalHeader: 'Registrar Viajero',
                buttonFooter: 'Guardar',
                disabled: false,
                showHide: false,
                data: data,
            })
        }
        else if (option === 2) {
            props.selectViajeroIdAction(data.id)
            setList({
                option: option,
                modal: true,
                modalHeader: 'Ver Viajero',
                buttonFooter: '',
                disabled: true,
                showHide: true,
                data: data,
            })
        }
        else if (option === 3) {
            props.selectViajeroIdAction(data.id)
            setList({
                option: option,
                modal: true,
                modalHeader: 'Editar Viajero',
                buttonFooter: 'Editar',
                disabled: false,
                showHide: false,
                data: data,
            })
        }
        else if (option === 4) {
            setList({
                option: option,
                modalAsignarViaje: true,
                modalHeader: 'Asignar Viaje',
                buttonFooter: 'Guardar',
                disabled: false,
                showHide: false,
                data: data,
            })
        }
    }

    const deleteViajero = (data) => {
        const message = {
            title: "Eliminar Viajero",
            info: "Los viajes asociados al viajero tambien seran eliminados ¿Esta seguro que desea eliminar el viajero?"
        };
        props.confirm(message, res => {
            if (res) {
                props.deleteViajeroAction(data);
            }
        });
    }

    return (
        <div className={classes.root}>
            {
                list.modal &&
                <DialogViajeros
                    option={list.option}
                    modal={list.modal}
                    modalHeader={list.modalHeader}
                    buttonFooter={list.buttonFooter}
                    disabled={list.disabled}
                    showHide={list.showHide}
                    data={list.data}
                    valorCloseModal={valorCloseModal}
                />
            }
            {
                list.modalAsignarViaje &&
                <DialogAsignarViaje
                    option={list.option}
                    modal={list.modalAsignarViaje}
                    modalHeader={list.modalHeader}
                    buttonFooter={list.buttonFooter}
                    disabled={list.disabled}
                    showHide={list.showHide}
                    data={list.data}
                    valorCloseModalAsignarViaje={valorCloseModalAsignarViaje}
                />
            }
            <div>
                <Typography variant="h5" color="textSecondary" align="center">Lista de Viajeros</Typography>
            </div>
            <div style={{ paddingTop: 20 }}>
                <Button
                    variant="contained"
                    color="primary"
                    onClick={() => { openModal(1); }}
                >
                    Agregar
                </Button>
            </div>
            {
                props.viajeros.data ?
                    <div style={{ paddingTop: 20 }}>
                        <Table aria-label="a dense table">
                            <TableHead className="">
                                <TableRow hover>
                                    <TableCell style={{ width: '10%' }} align="left">Nro</TableCell>
                                    <TableCell style={{ width: '10%' }} align="left">Cedula</TableCell>
                                    <TableCell style={{ width: '20%' }} align="left">Nombres</TableCell>
                                    <TableCell style={{ width: '15%' }} align="left">Fecha de Nacimiento</TableCell>
                                    <TableCell style={{ width: '15%' }} align="left">Telefono</TableCell>
                                    <TableCell style={{ width: '30%' }} align="left">Acciones</TableCell>
                                </TableRow>
                            </TableHead>
                            <TableBody>
                                <Fragment>
                                    {props.viajeros.data ? props.viajeros.data.map((data, i) => {
                                        return (
                                            <TableRow hover key={i}>
                                                <TableCell style={{ width: '10%' }} align="left">{i + 1}</TableCell>
                                                <TableCell style={{ width: '15%' }} align="left">{data.cedula}</TableCell>
                                                <TableCell style={{ width: '15%' }} align="left">{data.nombre}</TableCell>
                                                <TableCell style={{ width: '15%' }} align="left">{data.fecha_nacimiento}</TableCell>
                                                <TableCell style={{ width: '15%' }} align="left">{data.telefono}</TableCell>
                                                <TableCell style={{ width: '30%', minWidth: '225px' }} align="left">
                                                    <div className="" style={{ marginTop: '-24px', height: '10px' }}>
                                                        <IconButton aria-label="Delete"
                                                            title="Ver Viajero"
                                                            className="iconButtons"
                                                            onClick={() => { openModal(2, data); }}
                                                        >
                                                            <Visibility className="iconTable" />
                                                        </IconButton>
                                                        <IconButton aria-label="Delete"
                                                            title="Editar Viajero"
                                                            className="iconButtons"
                                                            onClick={() => { openModal(3, data); }}
                                                        >
                                                            <Edit className="iconTable" />
                                                        </IconButton>
                                                        <IconButton aria-label="Delete"
                                                            title="Eliminar Viajero"
                                                            className="iconButtons"
                                                            onClick={() => { deleteViajero(data); }}
                                                        >
                                                            <Delete className="iconTable" />
                                                        </IconButton>
                                                        <IconButton aria-label="Delete"
                                                            title="Asignar Viaje"
                                                            className="iconButtons"
                                                            onClick={() => { openModal(4, data); }}
                                                        >
                                                            <LocalAirport className="iconTable" />
                                                        </IconButton>
                                                    </div>
                                                </TableCell>
                                            </TableRow>
                                        );
                                    })
                                        :
                                        null
                                    }
                                </Fragment>
                            </TableBody>
                        </Table>
                    </div>
                    :
                    <div style={{ display: 'flex', justifyContent: 'center', alignItems: 'center', height: "60vh" }}>
                        <CircularProgress style={{ height: 60, width: 60, zIndex: 2 }} />
                    </div>
            }
        </div>
    );
}

const mapStateToProps = state => ({
    viajeros: state.viajeros,
});

const mapDispatchToProps = dispatch => ({
    selectAllViajerosActivosFunction: () => dispatch(selectAllViajerosActivosFunction()),
    selectViajeroIdAction: (id) => dispatch(selectViajeroIdAction(id)),
    deleteViajeroAction: (id) => dispatch(deleteViajeroAction(id)),
    confirm: (message, callback) => dispatch(openConfirmDialog(message, callback)),
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Viajeros);