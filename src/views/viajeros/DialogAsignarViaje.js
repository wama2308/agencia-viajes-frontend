import React, { useState, useEffect } from 'react';
import {
    CircularProgress,
    Dialog,
    DialogActions,
    DialogContent,
    DialogTitle,
    IconButton,
    Button,
    AppBar,
    Tabs,
    Tab,
    Typography,
    Box,
} from "@material-ui/core";
import PropTypes from 'prop-types';
import { Close } from "@material-ui/icons";
import { connect } from "react-redux";
import { saveViajeroViajesAction, cleanViajeAsignadoAction, selectAllViajerosViajesFunction } from "../../actions/ViajerosActions"
import { makeStyles, useTheme } from '@material-ui/core/styles';
import SwipeableViews from 'react-swipeable-views';
import AsignarViajes from "./AsignarViajes";
import ListViajesAsignados from "./ListViajesAsignados";
import { NotificationManager } from 'react-notifications';

function TabPanel(props) {
    const { children, value, index, ...other } = props;
    return (
        <Typography
            component="div"
            role="tabpanel"
            hidden={value !== index}
            id={`full-width-tabpanel-${index}`}
            aria-labelledby={`full-width-tab-${index}`}
            {...other}
        >
            {value === index && <Box p={1}>{children}</Box>}
        </Typography>
    );
}

TabPanel.propTypes = {
    children: PropTypes.node,
    index: PropTypes.any.isRequired,
    value: PropTypes.any.isRequired,
};

function a11yProps(index) {
    return {
        id: `full-width-tab-${index}`,
        'aria-controls': `full-width-tabpanel-${index}`,
    };
}

const useStyles = makeStyles((theme) => ({
    root: {
        backgroundColor: theme.palette.background.paper,
        width: 500,
    },
}));

const DialogAsignarViaje = props => {
    const classes = useStyles();
    const theme = useTheme();
    const [value, setValue] = useState(0);
    const [loading, setLoading] = useState('show')
    const [loadingTab, setLoadingTab] = useState('hide')

    useEffect(() => {
        if (props.option === 4) {
            setLoading('hide');
        }
    }, [props])

    const handleChangeTabs = (event, newValue) => {
        if (newValue === 1) {
            props.selectAllViajerosViajesFunction(props.data.id)
        }
        setValue(newValue);
    };

    const handleChangeTabsIndex = (index) => {
        setValue(index);
    };

    const closeModal = () => {
        props.valorCloseModalAsignarViaje();
        props.cleanViajeAsignadoAction();
    };

    const loadingShowAction = () => {
        setLoadingTab('hide');
        props.cleanViajeAsignadoAction();
        props.selectAllViajerosViajesFunction(props.data.id)
        handleChangeTabsIndex(1);
    }

    const validate = () => {
        let acum = "";
        if (props.viajeros.viajesAsignados.length === 0) {
            NotificationManager.warning('Debe agregar al menos un viaje');
            acum = 1;
        }
        if (acum > 0) {
            return false;
        }
        return true;
    }

    const handleViajeroViajes = event => {
        event.preventDefault();
        const isValid = validate();
        if (isValid) {
            setLoadingTab('show');
            saveViajeroViajesAction(
                { array_viajero_viaje: props.viajeros.viajesAsignados },
                () => { loadingShowAction() }
            );
        }
    }

    return (
        <Dialog
            fullWidth={true}
            maxWidth="md"
            open={props.modal}
            onClose={() => { closeModal() }}
            aria-labelledby="responsive-dialog-title"
            scroll="paper"
        >
            {loading === "hide" ? (
                <div>
                    <DialogTitle id="form-dialog-title">
                        <div style={{ display: 'flex' }}>
                            <div>
                                Pasajero: {props.data.nombre} - {props.data.cedula}
                            </div>
                            <div style={{ marginLeft: 'auto' }}>
                                <IconButton aria-label="Delete"
                                    className="iconButtons"
                                    onClick={() => { closeModal() }}
                                >
                                    <Close className="iconTable" />
                                </IconButton>
                            </div>
                        </div>
                    </DialogTitle>
                    <DialogContent dividers>
                        <AppBar position="static" color="default">
                            <Tabs
                                value={value}
                                onChange={handleChangeTabs}
                                indicatorColor="primary"
                                textColor="primary"
                                aria-label="full width tabs example"
                            >
                                <Tab label="Asignar Viaje(s)" {...a11yProps(0)} />
                                <Tab label="Viajes Reservados" {...a11yProps(1)} />
                            </Tabs>
                        </AppBar>
                        <SwipeableViews
                            axis={theme.direction === 'rtl' ? 'x-reverse' : 'x'}
                            index={value}
                            onChangeIndex={handleChangeTabsIndex}
                        >
                            <TabPanel value={value} index={0} dir={theme.direction}>
                                {
                                    loadingTab === 'hide' ?
                                        <AsignarViajes
                                            dataSelect={props.viajeros.viajes}
                                            dataViajero={props.data}
                                        />
                                        :
                                        <div style={{ height: "55vh" }}>
                                            <CircularProgress
                                                style={{
                                                    position: "fixed",
                                                    height: 40,
                                                    top: "45%",
                                                    right: "50%",
                                                    zIndex: 2
                                                }}
                                            />
                                        </div>
                                }

                            </TabPanel>
                            <TabPanel value={value} index={1} dir={theme.direction}>
                                <ListViajesAsignados dataViajeroViajes={props.viajeros.viajesGuardados} />
                            </TabPanel>
                        </SwipeableViews>
                    </DialogContent>
                    <DialogActions>
                        <Button
                            variant="contained"
                            color="default"
                            onClick={() => closeModal()}
                        >
                            Cancelar
                        </Button>
                        {
                            !props.showHide &&
                            <Button
                                variant="contained"
                                color="primary"
                                onClick={handleViajeroViajes}
                            >
                                {props.buttonFooter}
                            </Button>
                        }

                    </DialogActions>
                </div>
            ) : (
                    <div style={{ height: "55vh" }}>
                        <CircularProgress
                            style={{
                                position: "fixed",
                                height: 40,
                                top: "45%",
                                right: "50%",
                                zIndex: 2
                            }}
                        />
                    </div>
                )
            }
        </Dialog>
    );
}

const mapStateToProps = state => ({
    viajeros: state.viajeros,
});

const mapDispatchToProps = dispatch => ({
    cleanViajeAsignadoAction: () => dispatch(cleanViajeAsignadoAction()),
    selectAllViajerosViajesFunction: (id) => dispatch(selectAllViajerosViajesFunction(id)),
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(DialogAsignarViaje);