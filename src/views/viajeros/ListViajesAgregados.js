import React, { Fragment } from 'react';
import {
    IconButton,
    Typography,
    Table,
    TableBody,
    TableCell,
    TableHead,
    TableRow,
} from "@material-ui/core";
import { Delete } from "@material-ui/icons";

const ListViajesAgregados = (props) => {
    return (
        <div>
            <Typography variant="h6" color="textSecondary" align="center">Lista de Viajes Agregados</Typography>
            <Table aria-label="a dense table">
                <TableHead className="">
                    <TableRow hover>
                        <TableCell style={{ width: '25%' }} align="left">Nro</TableCell>
                        <TableCell style={{ width: '50%' }} align="left">Viaje</TableCell>
                        <TableCell style={{ width: '25%' }} align="left">Acciones</TableCell>
                    </TableRow>
                </TableHead>
                <TableBody>
                    <Fragment>
                        {props.viajesAsig ? props.viajesAsig.map((data, i) => {
                            return (
                                <TableRow hover key={i}>
                                    <TableCell style={{ width: '25%' }} align="left">{i + 1}</TableCell>
                                    <TableCell style={{ width: '50%' }} align="left">{data.viaje}</TableCell>
                                    <TableCell style={{ width: '25%', minWidth: '225px' }} align="left">
                                        <div className="" style={{ marginTop: '-24px', height: '10px' }}>
                                            <IconButton aria-label="Delete"
                                                title="Eliminar viaje agregado"
                                                className="iconButtons"
                                                onClick={() => { props.deleteViajeAsignadoAction(i); }}
                                            >
                                                <Delete className="iconTable" />
                                            </IconButton>
                                        </div>
                                    </TableCell>
                                </TableRow>
                            );
                        })
                            :
                            null
                        }
                    </Fragment>
                </TableBody>
            </Table>
        </div>
    );
}

export default ListViajesAgregados;