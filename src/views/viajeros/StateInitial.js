import { formatFechaAct } from "../../core/helpers";

export const stateInitial = {
    loading: 'show',
    actionReducer: 0,
    cedula: "",
    cedulaError: false,
    cedulaTextError: '',
    nombres: "",
    nombresError: false,
    nombresTextError: '',
    fechaNacimiento: formatFechaAct(new Date()),
    fechaNacimientoError: false,
    fechaNacimientoTextError: '',
    telefono: "",
    telefonoError: false,
    telefonoTextError: '',    
}