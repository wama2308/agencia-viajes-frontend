import React, { useState, useEffect } from 'react';
import "../../assets/css/style.css";
import {
    IconButton,
    Typography,
} from "@material-ui/core";
import Select from 'react-select';
import { AddCircleOutline } from "@material-ui/icons";
import { connect } from "react-redux";
import ListViajesAgregados from "./ListViajesAgregados";
import { asignarViajesAction, deleteViajeAsignadoAction } from '../../actions/ViajerosActions';

const AsignarViajes = (props) => {
    const [selectViajes, setSelectViajes] = useState(null)
    const [divError, setDivError] = useState('')
    const [textError, setTextError] = useState('')

    const handleChangeSelect = (value) => {
        setSelectViajes(value);
        setDivError('');
        setTextError('');
    };

    const addViaje = () => {
        if (selectViajes === null) {
            setDivError('borderColor');
            setTextError('Seleccione un viaje');
        } else {
            let dataArray = {
                viajero_id: props.dataViajero.id,
                viaje_id: selectViajes.value,
                viaje: selectViajes.label,
            }
            props.asignarViajesAction(dataArray)
            setSelectViajes(null);            
        }
    }
    
    return (
        <div
            style={{
                minHeight: '40vh',
                maxHeight: '70vh',
                display: 'flex',
                padding: 15,
                flexDirection: 'column'
            }}
        >
            <div>
                <Typography>Seleccione el viaje:</Typography>
            </div>
            <div style={{ display: 'flex', flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
                <div style={{ flexGrow: 1 }}>
                    <div className={divError}>
                        <Select
                            isSearchable
                            isClearable
                            value={selectViajes}
                            onChange={event => handleChangeSelect(event)}
                            options={props.dataSelect}
                        />
                    </div>
                    <div className='errorControl'>
                        {textError}
                    </div>
                </div>
                <div style={{ flexGrow: 1 }}>
                    <IconButton aria-label="Delete"
                        title="Agregar Viaje"
                        className="iconButtons"
                        onClick={addViaje}
                    >
                        <AddCircleOutline className="iconTable" style={{ fontSize: 35 }} />
                    </IconButton>
                </div>
            </div>
            <div style={{ paddingTop: 25 }}>
                <ListViajesAgregados
                    viajesAsig={props.viajeros.viajesAsignados}
                    deleteViajeAsignadoAction={props.deleteViajeAsignadoAction}
                />
            </div>
        </div>
    );
}

const mapStateToProps = state => ({
    viajeros: state.viajeros,
});

const mapDispatchToProps = dispatch => ({
    asignarViajesAction: (data) => dispatch(asignarViajesAction(data)),
    deleteViajeAsignadoAction: (index) => dispatch(deleteViajeAsignadoAction(index)),
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(AsignarViajes);